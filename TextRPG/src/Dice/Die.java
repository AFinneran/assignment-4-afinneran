package Dice;
import java.util.Random;

public class Die {
	private final Random random = new Random();
	public int max;
	public int result;
	
	public int roll(int newMax) {
		result = 1 + random.nextInt(newMax);
		return result;
	}
}