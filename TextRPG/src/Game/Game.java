package Game;
import Characters.*;
import Game.Instances.Store;
import Game.Instances.Disaster;
import Dice.Die;
import Game.Instances.Fortune;

public class Game {
	Die dice = new Die();
	public Game(Player playerOne) {
		playerOne.printHealth();
		playerOne.printAttack();
		playerOne.printDefense();
		playerOne.printSpeed();
		playerOne.printGold();
		System.out.println("You travel down the path");
		System.out.println("------------------------");
		generateInstance();
	}
	public generateInstance(){

		int event = dice.roll(3);
		if(event == 1) {
			Disaster distasterInstance = new Disaster(playerOne);
		}
		else if (event == 2){
			Store storeInstance = new Store(playerOne);
		}
		else if (event == 3) {
			Fortune fortuneInstance = new Fortune(playerOne);
		}

	}
}
