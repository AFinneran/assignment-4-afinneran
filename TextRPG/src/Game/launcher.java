package Game;

import Characters.Player;
import Dice.*;
import Game.Game;

public class launcher {
	
	public static void main(String[] args) {
		Die dice = new Die();
		Player playerOne = new Player(10, dice.roll(10), dice.roll(10), dice.roll(10), 100);
		Game gamePlay = new Game(playerOne);
	}

}
