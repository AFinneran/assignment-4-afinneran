package Game.Instances;

import java.util.Scanner;

import Characters.Player;
import Dice.Die;

public class Fortune {
	Player playerOne = new Player(0,0,0,0,0);
	Die dice = new Die();
	Scanner scan = new Scanner(System.in);
	
	public Fortune(Player playerOneStatGetter) {
		playerOne.setHealth(playerOneStatGetter.getHealth());
		playerOne.setAttack(playerOneStatGetter.getAttack());
		playerOne.setDefense(playerOneStatGetter.getDefense());
		playerOne.setSpeed(playerOneStatGetter.getSpeed());
		playerOne.setGold(playerOneStatGetter.getGold());
		randomFortune();
	}
	
	public void randomFortune(){
		int fortune = dice.roll(3);
		if(fortune == 1) {
			System.out.println("You stub your toe on a roc- coin purse?? You dont see anyone in sight lucky you +100 gold");
			playerOne.changeGold(100);
			Buffer bufferPlayerStat = new Buffer(playerOne);
		}
		else if (fortune == 2) {
			System.out.println("While answering natures call you notice a pair of glasses and pick them up. A few feet down the road an old lady approaches you and asks if you have seen her glasses? (1 for yes, 0 for no)");
			int answer = scan.nextInt();
			if (answer==1) {
				System.out.println("The women smiles, you hand her the glasses, and she hands you 50 gold");
				playerOne.changeGold(50);
				Buffer bufferPlayerStat = new Buffer(playerOne);
			}
			else if (answer == 0) {
				System.out.println("The women explains that she saw you pick them up, calls you lots of names, and hits you with her cane. *You hand her the glasses and she storms off*");
				Buffer bufferPlayerStat = new Buffer(playerOne);
			}
		}
		else if (fortune == 3) {
			System.out.println("You notice an apple on a tree as you are passing you eat it and heal your health +3");
			p1.changeHealth(3);
			Buffer bufferPlayerStat = new Buffer(playerOne);
		}
	}

}
