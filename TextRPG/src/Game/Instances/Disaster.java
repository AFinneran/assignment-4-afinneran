package Game.Instances;
import Characters.Player;
import Game.Instances.Buffer;
import Dice.Die;
import java.util.Scanner;

public class Disaster {
	Player playerOne = new Player(0,0,0,0,0);
	Die dice = new Die();
	Scanner scan = new Scanner(System.in);
	
	public Disaster(Player playerOneStatGetter) {
		playerOne.setHealth(playerOneStatGetter.getHealth());
		playerOne.setAttack(playerOneStatGetter.getAttack());
		playerOne.setDefense(playerOneStatGetter.getDefense());
		playerOne.setSpeed(playerOneStatGetter.getSpeed());
		playerOne.setGold(playerOneStatGetter.getGold());
		randomDisaster();
	}
	
	public void randomDisaster() {
		int result = dice.roll(3);
		if(result == 1) {
			System.out.println("A mystical force surrounds you, and in your head you hear an unfamilier voice that asks 'Traveller do you wish to gain the wisdom of the mystics??' (1 for yes, and 0 for no)");
			int answer = scan.nextInt();
			if(answer == 1) {
				System.out.println("Your wisdom is to not trust a mystical spirit *Your shield evaporates -2 defense");
				playerOne.changeDefense(-2);
				Buffer bufferPlayerStat = new Buffer(playerOne);
			}
			else if(answer == 0) {
				System.out.println("How dare you insult a mystical spirit!!! *You feel your muscles weaken -2 attack*");
				playerOne.changeAttack(-2);
				Buffer bufferPlayerStat = new Buffer(playerOne);
			}
			else {
				System.out.println("The mystical force disapears, and you hesitantly get yourself settled for a nap. Were you going crazy?");
				Buffer bufferPlayerStat = new Buffer(playerOne);
			}
		}
		else if (result == 2) {
			System.out.println("You stub your toe on a rock - 3 speed be more careful");
			playerOne.changeSpeed(-3);
			Buffer bufferPlayerStat = new Buffer(playerOne);			
		}
		else if (result == 3) {
			System.out.println("An archer pops out of no where and shoots you in the hand. You spin around like a top, and cut him to pieces. You remove the arrow OUCH! that hurt!!! -1 health");
			playerOne.changeHealth(-1);
			Buffer bufferPlayerStat = new Buffer(playerOne);
		}
	}
	
}
