package Game.Instances;
import Characters.Player;
import java.util.Scanner;
import Game.Instances.Buffer;

public class Store {
	Scanner scan = new Scanner(System.in);
	Player playerOne = new Player(0,0,0,0,0);
	public Store(Player playerOneStatGetter) {
		playerOne.setHealth(playerOneStatGetter.getHealth());
		playerOne.setAttack(playerOneStatGetter.getAttack());
		playerOne.setDefense(playerOneStatGetter.getDefense());
		playerOne.setSpeed(playerOneStatGetter.getSpeed());
		playerOne.setGold(playerOneStatGetter.getGold());
		menu();
	}
	
	private void menu() {
		System.out.println("Welcome to my shop I am the most level headed shopkeep in the land! Would You Like to Browse? (1 for yes, 0 for no) ");
		playerOne.printGold();
		int response;
		response = scan.nextInt();
		if(response == 1) {
			buy();
		}
		else if(response == 0) {
			exit(0);
		}
		else {
			System.out.println("You are wasting my time leave my store!");
			exit(1);
		}
	}
	
	private void buy() {
		System.out.println("Make a selection below buy pressing the number you can only buy one item, or press 0 to exit!");
		System.out.println("[1] Sword +2 Attack 100 Gold");
		System.out.println("[2] Sheild +2 Defense 200 Gold");
		System.out.println("[3] Boots of Mobility +2 Speed 300 Gold");
		int response;
		response = scan.nextInt();
		if(response == 1) {
			if(playerOne.getGold()>=100) {
				playerOne.changeGold(-100);
				playerOne.changeAttack(2);
				System.out.print("Congrats on your new Sword! ");
				playerOne.printAttack();
				System.out.print("Your remaining ");
				playerOne.printGold();
				exit(0);
			}
			else {
				System.out.println("You do not have enough gold get out of my store!");
				exit(1);
			}
		}
		if(response == 2) {
			if(playerOne.getGold()>=200) {
				playerOne.changeGold(-200);
				playerOne.changeDefense(2);
				System.out.print("Congrats on your new Sheild! ");
				playerOne.printDefense();
				System.out.print("Your remaining ");
				playerOne.printGold();
				exit(0);
			}
			else {
				System.out.println("You do not have enough gold get out of my store!");
				exit(1);
			}
		}
		if(response == 3) {
			if(playerOne.getGold()>=300) {
				playerOne.changeGold(-300);
				playerOne.changeSpeed(2);
				System.out.print("Congrats on your new Boots! ");
				playerOne.printSpeed();
				System.out.print("Your remaining ");
				playerOne.printGold();
				exit(0);
			}
			else {
				System.out.println("You do not have enough gold get out of my store!");
				exit(1);
			}
		}
		else {
			System.out.println("You are wasting my time leave!");
			exit(1);
		}
	}

	private void exit(int e) {
		if(e == 0){
			System.out.println("You exit the shop happy as a peach");
			System.out.println(" ");
			Buffer bufferPlayerStat = new Buffer(playerOne);
			}
		else if(e == 1) {
			System.out.println("You leave the store scared for your life as the shopkeep reaches for his sword! Better answer him correctly next time!");
			System.out.println(" ");
			Buffer bufferPlayerStat = new Buffer(playerOne);
		}
		
	}
}


