package Characters;


public abstract class Character {
	public int health;
	public int attack;
	public int defense;
	public int speed;
	
	public Character(int startHealth, int startAttack, int startDefense, int startSpeed) {
		health = startHealth;
		attack = startAttack;
		defense = startDefense;
		speed = startSpeed;
	}
	
	public int getHealth() {
		return health;
	}
	
	public void printHealth() {
		System.out.print("Your Health is: ");
		System.out.println(health);
	}
	
	public void setHealth(int totHealth) {
		health = totHealth;
	}
	
	public void changeHealth(int addedHealth) {
		health += addedHealth;
		if (health<1) { //Health can not be lower then 1
			health = 1;
		}
		if (health>10) { //HEalth can not be higher then 10
			health = 10;
		}
	}
	
	public int getAttack() {
		return attack;
	}
	
	public void printAttack() {
		System.out.print("Your Attack is: ");
		System.out.println(attack);
	}
	
	public void setAttack(int totAttack) {
		attack = totAttack;
	}
	
	public void changeAttack(int addedAttack) {
		attack += addedAttack;
		if (attack<1) { //Attack can not be lower then 1
			attack = 1;
		}
		if (attack>10) { //Attack can not be higher then 10
			attack = 10;
		}
	}
	
	public int getDefense() {
		return defense;
	}
	
	public void printDefense() {
		System.out.print("Your Defense is: ");
		System.out.println(defense);
	}
	
	public void setDefense(int totDefense) {
		defense = totDefense;
	}
	
	public void changeDefense(int addedDefense) {
		defense += newValue;
		if (defense<1) { //Defense can not be lower then 1
			defense = 1;
		}
		if (defense>10) { //Defense can not be higher then 10
			defense = 10;
		}
	}

	public int getSpeed() {
		return speed;
	}
	
	public void printSpeed() {
		System.out.print("Your Speed is: ");
		System.out.println(speed);
	}
	
	public void setSpeed(int totSpeed) {
		speed = totSpeed;
	}	

	public void changeSpeed(int addedSpeed) {
		speed += addedSpeed;
		if (speed<1) { //Speed can not be lower then 1
			speed = 1;
		}
		if (speed>10) { //Speed can not be higher then 10
			speed = 10;
		}
	}
}

