package Characters;

public class Player extends Character{
	
	public int gold;
	
	public Player(int startHealth, int startAttack, int startDefense, int startSpeed, int startGold) {
		super(startHealth, startAttack, startDefense, startSpeed);
		gold = startGold;
	}
	
	public void setGold(int totGold) {
		gold = totGold;
	}
	
	public int getGold() {
		return gold;
	}
	
	public void printGold() {
		System.out.print("Gold Pieces: ");
		System.out.println(gold);
	}
	
	public void changeGold(int changedGold) {
		gold += changedGold;
		if (gold<1) { //Gold can not be lower then 1
			gold = 0;
		}
		if (gold>9999) { //Gold can not be higher then 9999
			gold = 9999;
		}
	}

	public void isAlive(Player playerOne) {
		int life = playerOne.getHealth();
		if (life == 0) {
			System.out.println("YOU DIED, BUT HOW THIS GAME IS SO EASY!");
		}
		else {
			return;
		}
}
}